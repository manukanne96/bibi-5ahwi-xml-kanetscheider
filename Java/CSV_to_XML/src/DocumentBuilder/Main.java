package DocumentBuilder;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class Main {

	private static ArrayList<String> readCSV(String pathCSV) throws FileNotFoundException{
		ArrayList<String> list = new ArrayList<>();
		Scanner s = new Scanner(new File(pathCSV));
		while (s.hasNextLine()) {
			list.add(s.nextLine());
		}
		s.close();
		return list;
	}
	
	public static void main(String[] args) throws ParserConfigurationException, FileNotFoundException, TransformerException {
		
		final String pathCSV = "C:\\Users\\Manuel Kanetscheider\\Documents\\HTL\\5AHW\\Koelloe XML\\Java\\CSV_to_XML\\src\\Daten\\shischa.csv";
		final String pathXML = "C:\\Users\\Manuel Kanetscheider\\Documents\\HTL\\5AHW\\Koelloe XML\\Java\\CSV_to_XML\\src\\Daten\\Daten.xml";
		//------------------------------ Dokument 'bauen' -----------------------------------------------
		DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dokumentBuilder = documentFactory.newDocumentBuilder();
		
		Document doc = (Document) dokumentBuilder.newDocument();
		Element rootKnoten = doc.createElement("ShischaLounge");
		doc.appendChild(rootKnoten);
		
		ArrayList<String> datenCSV = readCSV(pathCSV);
		for (String line :  datenCSV)
		{
			String tisch = line.split(";")[0];
			String shischa = line.split(";")[1];
			String tabak = line.split(";")[2];
			String preis = line.split(";")[3];
			
			Element neuerTisch = doc.createElement("Tisch");
			
			Element tischNr = doc.createElement("TischNr");
			tischNr.appendChild(doc.createTextNode(tisch));
			neuerTisch.appendChild(tischNr);
			
			Element shischaKnoten = doc.createElement("Shischa");
			shischaKnoten.appendChild(doc.createTextNode(shischa));
			neuerTisch.appendChild(shischaKnoten);
			
			Element tabakKnoten = doc.createElement("Tabak");
			tabakKnoten.appendChild(doc.createTextNode(tabak));
			neuerTisch.appendChild(tabakKnoten);
			
			Element preisKnoten = doc.createElement("Preis");
			preisKnoten.appendChild(doc.createTextNode(preis));
			neuerTisch.appendChild(preisKnoten);
			
			rootKnoten.appendChild(neuerTisch);
		}
		
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(new File(pathXML));
		transformer.transform(source, result);

	}

}
