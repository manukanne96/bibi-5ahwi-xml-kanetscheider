import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class CSV_to_XML_Formatter {

	private String pathCSV;
	private String pathXML;

	public CSV_to_XML_Formatter(String pathCSV, String pathXML) {
		this.pathCSV = pathCSV;
		this.pathXML = pathXML;
	}

	private ArrayList<String> readCSV() throws FileNotFoundException {
		ArrayList<String> list = new ArrayList<>();
		Scanner s = new Scanner(new File(pathCSV));
		while (s.hasNextLine()) {
			list.add(s.nextLine());
		}
		s.close();
		return list;
	}

	public void writeToXML() throws IOException{
		ArrayList<String> list = readCSV();
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("<ShischaLounge>\n");
		for (String line : list)
		{
			String tisch = line.split(";")[0];
			String shischa = line.split(";")[1];
			String tabak = line.split(";")[2];
			String preis = line.split(";")[3];
			sBuilder.append("\t<Tisch>\n");
			sBuilder.append("\t\t<TischNr>"+tisch + "</TischNr>\n");
			sBuilder.append("\t\t<Shischa>" + shischa + "</Shischa>\n");
			sBuilder.append("\t\t<Tabak>" + tabak + "</Tabak>\n");
			sBuilder.append("\t\t<Preis>" + preis + "</Preis>\n");
			sBuilder.append("\t</Tisch>\n");
		}
		sBuilder.append("</ShischaLounge>");
		FileWriter fw = new FileWriter(new File(pathXML));
		fw.write(sBuilder.toString());
		fw.close();		
	}

}
