<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<html>
			<body>
				<h2>Dokument</h2>
				<xsl:for-each select="//paragraph">
					<p>
						<xsl:for-each select="character">

							<xsl:choose>
								<xsl:when test="@attr > 0">
									<c style="background-color: lightpink"><xsl:value-of select="@value" /></c>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="@value" />
								</xsl:otherwise>
							</xsl:choose>

						</xsl:for-each>
					</p>
				</xsl:for-each>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>