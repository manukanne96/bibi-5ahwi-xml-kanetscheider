<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<html>
			<body>
				<h2>Shischa</h2>
				<table border="1">
					<tr>
						<th>Tisch Nummer</th>
						<th>Shischa</th>
					</tr>
					<xsl:for-each select="ShischaLounge/Tisch">
						<tr>
							<td>
								<xsl:value-of select="TischNr" />
							</td>
							<td>
								<xsl:value-of select="Shischa" />
							</td>
						</tr>
					</xsl:for-each>
				</table>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>