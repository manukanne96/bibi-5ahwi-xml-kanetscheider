﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:template match="/">
    <h1>
      <xsl:value-of select="//location/country"/>
    </h1>
    <h2>
      <xsl:value-of select="//location/name"/>
    </h2>
    <h3>Precipitation</h3>
    <table border="1">
      <tr>
        <td>From</td>
        <td>To</td>
        <td>Unit</td>
        <td>Value</td>
        <td>Type</td>
      </tr>
      <xsl:for-each select="//forecast/*">
        <xsl:if test="precipitation/@unit != ''">
          <tr>
            <td>
              <xsl:value-of select="@from"/>
            </td>
            <td>
              <xsl:value-of select="@to"/>
            </td>
            <td>
              <xsl:value-of select="precipitation/@unit"/>
            </td>
            <td>
              <xsl:value-of select="precipitation/@value"/>
            </td>
            <td>
              <xsl:value-of select="precipitation/@type"/>
            </td>
          </tr>
        </xsl:if>
      </xsl:for-each>
    </table>
    <h3>Wind</h3>
    <table border="1">
      <tr>
        <td>From</td>
        <td>To</td>
        <td>Deg</td>
        <td>Code</td>
        <td>Direction</td>
        <td>Speed</td>
      </tr>
      <xsl:for-each select="//forecast/*">
        <tr>
          <td>
            <xsl:value-of select="@from"/>
          </td>
          <td>
            <xsl:value-of select="@to"/>
          </td>
          <td>
            <xsl:value-of select="windDirection/@deg"/>
          </td>
          <td>
            <xsl:value-of select="windDirection/@code"/>
          </td>
          <td>
            <xsl:value-of select="windDirection/@name"/>
          </td>
          <td>
            <xsl:value-of select="windSpeed/@mps"/>
          </td>
        </tr>
      </xsl:for-each>
    </table>
    <h3>Temperature</h3>
    <table border="1">
      <tr>
        <td>From</td>
        <td>To</td>
        <td>Minimum</td>
        <td>Maximum</td>
      </tr>
      <xsl:for-each select="//forecast/*">
        <tr>
          <td>
            <xsl:value-of select="@from"/>
          </td>
          <td>
            <xsl:value-of select="@to"/>
          </td>
          <td>
            <xsl:value-of select="temperature/@min"/>
          </td>
          <td>
            <xsl:value-of select="temperature/@max"/>
          </td>
        </tr>
      </xsl:for-each>
    </table>
    <h3>Other Information</h3>
    <table border="1">
      <tr>
        <td>From</td>
        <td>To</td>
        <td>Pressure</td>
        <td>Humidity</td>
        <td>Clouds</td>
      </tr>
      <xsl:for-each select="//forecast/*">
        <tr>
          <td>
            <xsl:value-of select="@from"/>
          </td>
          <td>
            <xsl:value-of select="@to"/>
          </td>
          <td>
            <xsl:value-of select="pressure/@value"/>
          </td>
          <td>
            <xsl:value-of select="humidity/@value"/>
          </td>
          <td>
            <xsl:value-of select="clouds/@value"/>
          </td>
        </tr>
      </xsl:for-each>
    </table>
  </xsl:template>
</xsl:stylesheet>
