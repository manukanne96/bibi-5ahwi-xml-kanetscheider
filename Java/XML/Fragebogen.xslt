﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <xsl:for-each select="/questionnaire/*">

      <h1>
        <xsl:value-of select="@id"/>
        <xsl:choose>
          <xsl:when test="name(.)='header'"></xsl:when>
          <xsl:otherwise>.</xsl:otherwise>
        </xsl:choose>
        <xsl:value-of select="text"/>
      </h1>
      <h3>
        <xsl:value-of select="qdescription"/>
      </h3>
      <br></br>
      
      <xsl:if test="name(.)='closedEndedQ'">
        <form>
          <xsl:for-each select="choices/*">
            <input type="checkbox" name="">
              <xsl:value-of select="@id"/>.
              <xsl:value-of select="text"/>
              <br></br>
            </input>         
          </xsl:for-each>       
        </form>
      </xsl:if>

      <xsl:if test="name(.)='openendedMatrixQ'">
        <table>
          <xsl:for-each select="questions/*">
            <tr>
              <td>
                <xsl:value-of select="text"/>
              </td>
              <td>
                <input type="textfiled"></input>
              </td>
            </tr>
          </xsl:for-each>
        </table>
      </xsl:if>

      <xsl:if test="name(.)='questionMatrixMult'">
        <table border="1">
          <xsl:for-each select="questions/*">
            <tr>
              <td>
                <xsl:value-of select="text"/>
              </td>
              <xsl:for-each select="../choices/*">
                  <td>
                    <input type="radio"></input>
                    <xsl:value-of select="text"/>
                  </td>
              </xsl:for-each>
            </tr>
          </xsl:for-each>
          
        </table>
      </xsl:if>
      
    </xsl:for-each>
  </xsl:template>
</xsl:stylesheet>