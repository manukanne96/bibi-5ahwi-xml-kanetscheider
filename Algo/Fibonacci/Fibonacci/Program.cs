﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Fibonacci
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Geben Sie ein Limit ein: ");
            long limit = 0;
            while (!long.TryParse(Console.ReadLine(), out limit)) { }
            
            var fibonacci = new List<long>();
            fibonacci.Add(1);
            fibonacci.Add(1);

            Stopwatch watch = new Stopwatch();
            watch.Start();
            while (true)
            {
                long number = fibonacci[fibonacci.Count - 1] + fibonacci[fibonacci.Count - 2];
                if (number > limit)
                    break;
                fibonacci.Add(number);
            }
            watch.Stop();

            Console.WriteLine("---------------------------------------------");
            foreach (var z in fibonacci)
                Console.WriteLine(z);
            Console.WriteLine("---------------------------------------------");
            Console.WriteLine("Benötigte Zeit [ms]: " + watch.ElapsedMilliseconds);
            Console.ReadLine();
        }
    }
}
