﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Palindrom
{
    class Program
    {
        static bool isPalindrom(string text)
        {
            if (string.IsNullOrEmpty(text) || text.Length == 1)
                return true;

            if (text.First() == text.Last())
            {
                text = text.Remove(0, 1);
                text = text.Remove(text.Length - 1, 1);
                isPalindrom(text);
                return true;
            }
            return false;

        }

        static void Main(string[] args)
        {
            Console.WriteLine("Geben Sie einen Text ein: ");
            string text = Console.ReadLine().Trim().ToLower();
            bool b = isPalindrom(text);
            Console.WriteLine("Ist Palindrom: {0}", b);
            Console.Read();
        }
    }
}
