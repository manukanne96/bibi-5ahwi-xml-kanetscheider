﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rekursion
{
    class Program
    {
        static void fibonacci(int z1, int z2, int limit = 100)
        {
            int result = z1 + z2;
            Console.WriteLine(z2);
            if (result <= limit)
            {
                fibonacci(z2, result);
            }
        }

        static void Main(string[] args)
        {
            fibonacci(1, 1);
            Console.Read();
        }
    }
}
